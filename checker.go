package kratos_healthcheck

import (
	"context"
	"time"
)

type Checker struct {
	name    string
	timeout time.Duration
	check   func(ctx context.Context) error
	group   string
}

func NewChecker(options ...CheckOption) *Checker {
	defaultChecker := &Checker{
		timeout: 3 * time.Second,
	}

	if len(options) > 0 {
		for _, option := range options {
			option(defaultChecker)
		}
	}

	return defaultChecker
}

func (cker *Checker) Check() ItemDetail {
	ctx := context.Background()

	ctx, cancel := context.WithTimeout(ctx, cker.timeout)
	defer cancel()

	err := cker.check(ctx)
	var result ItemDetail
	result.Name = cker.name
	result.group = cker.group
	result.Err = ""
	result.Status = UP.String()
	result.status = UP
	if err != nil {
		result.Err = err.Error()
		result.Status = DOWN.String()
		result.status = DOWN
	}

	return result
}
