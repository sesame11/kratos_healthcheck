package kratos_healthcheck

import (
	"context"
	"time"
)

type CheckOption func(checker *Checker)

func WithName(name string) CheckOption {
	return func(checker *Checker) {
		checker.name = name
	}
}

func WithTimeout(timeout time.Duration) CheckOption {
	return func(checker *Checker) {
		checker.timeout = timeout
	}
}

func WithCheck(check func(ctx context.Context) error) CheckOption {
	return func(checker *Checker) {
		checker.check = check
	}
}

func WithGroup(group string) CheckOption {
	return func(checker *Checker) {
		checker.group = group
	}
}
