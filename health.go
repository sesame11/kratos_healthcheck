package kratos_healthcheck

import "sync"

type HealthStatus bool

const (
	UP   HealthStatus = true
	DOWN HealthStatus = false
)

func (status HealthStatus) String() string {
	if status {
		return "up"
	} else {
		return "down"
	}
}

type HealthCheck struct {
	checkers []*Checker
	async    bool
}

type Result struct {
	Status string
	Items  []GroupDetail
	status HealthStatus
}

type GroupDetail struct {
	Status string
	Group  string
	status HealthStatus
	Items  []ItemDetail
}

type ItemDetail struct {
	Status string
	Name   string
	Err    string
	group  string
	status HealthStatus
}

func NewHealthCheck(checkers []*Checker, async bool) *HealthCheck {
	return &HealthCheck{
		checkers: checkers,
		async:    async,
	}
}

func (healthCheck *HealthCheck) Start() Result {

	var resultDetailList []ItemDetail
	if healthCheck.async {
		resultDetailList = asyncCheck(healthCheck.checkers)
	} else {
		resultDetailList = syncCheck(healthCheck.checkers)
	}

	return combineResultDetail(resultDetailList)
}

func combineResultDetail(items []ItemDetail) Result {
	groupDetail := make(map[string]GroupDetail)

	for _, item := range items {
		if groupDetailItem, ok := groupDetail[item.group]; ok {
			groupDetailItem.status = groupDetailItem.status && item.status
			groupDetailItem.Items = append(groupDetailItem.Items, item)
			groupDetail[item.group] = groupDetailItem
		} else {
			groupDetailItem := NewGroupDetail()
			groupDetailItem.Group = item.group
			groupDetailItem.Items = append(groupDetailItem.Items, item)
			groupDetailItem.status = item.status
			groupDetail[item.group] = groupDetailItem
		}
	}

	groupDetailList := make([]GroupDetail, len(groupDetail))
	var result Result
	result.status = UP
	i := 0
	for _, groupDetailItem := range groupDetail {
		groupDetailItem.Status = groupDetailItem.status.String()
		result.status = result.status && groupDetailItem.status
		groupDetailList[i] = groupDetailItem
		i++
	}
	result.Items = groupDetailList
	result.Status = result.status.String()

	return result
}

func asyncCheck(checkers []*Checker) []ItemDetail {
	var wg sync.WaitGroup
	checkerLen := len(checkers)
	result := make([]ItemDetail, checkerLen)
	for i := 0; i < len(checkers); i++ {
		wg.Add(1)
		go func(ck *Checker, index int, wg *sync.WaitGroup) {
			result[index] = ck.Check()
			wg.Done()
		}(checkers[i], i, &wg)
	}

	wg.Wait()

	return result
}

func syncCheck(checkers []*Checker) []ItemDetail {
	result := make([]ItemDetail, len(checkers))

	for index, checker := range checkers {
		result[index] = checker.Check()
	}

	return result
}

func NewGroupDetail() GroupDetail {
	var groupDetail GroupDetail
	groupDetail.Items = make([]ItemDetail, 0)

	return groupDetail
}

func (result Result) IsUp() bool {
	return result.status == UP
}
